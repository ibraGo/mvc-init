<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>List de produit</title>
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="${pageContext.request.contextPath}/">List</a></li>
        <li><a href="${pageContext.request.contextPath}/create">create</a></li>
        
      </ul>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">libelle</th>
      <th scope="col">prix</th>
      <th scope="col">type</th>
      <th scope="col">actions</th>
    </tr>
  </thead>
  <tbody>
  <c:forEach var="produit" items="${produits}">
    <tr>
      <th scope="row"><c:out value="${produit.id}"/></th>
      <td><c:out value="${produit.libelle}"/></td>
      <td><c:out value="${produit.prix}"/></td>
      <td><c:out value="${produit.type}"/></td>
      <td>
      	<a href="${pageContext.request.contextPath}/edit/${produit.id}">modifier</a><br>
      	<a href="${pageContext.request.contextPath}/delete/${produit.id}">supprimer</a>
      </td>
    </tr>
   </c:forEach>

  </tbody>
</table>


</body>
</html>