<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Modification de produit</title>
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" rel="stylesheet">
</head>
<body>
	<form:form action="${pageContext.request.contextPath}/saveEdit" modelAttribute="produit"
		method="post">
		<form:hidden path="id" />
		<div class="form-group">
			<label>libelle : </label>
			<form:input path="libelle" cssClass="form-control" />
		</div>
		<div class="form-group">
			<label>Prix : </label>
			<form:input path="prix" cssClass="form-control" />
		</div>
		<div class="form-group">
			<label>Type : </label>
			<form:select path="type" cssClass="form-control">
				<form:options />
			</form:select>
		</div>
		<input type="submit" value="Modifier" />
		<form:errors path="libelle" cssClass="alert alert-danger"
			cssStyle="display:block;margin-top:60px;text-align:left;"></form:errors>
	</form:form>

</body>
</html>