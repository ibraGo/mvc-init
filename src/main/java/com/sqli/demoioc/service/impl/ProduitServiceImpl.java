package com.sqli.demoioc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sqli.demoioc.dao.IProduitDao;
import com.sqli.demoioc.model.Produit;
import com.sqli.demoioc.service.IProduitService;

@Component
public class ProduitServiceImpl implements IProduitService{

	
	@Autowired
	private IProduitDao produitDao;

	@Override
	public List<Produit> getAll() {
		return produitDao.getAll();
	}

	@Override
	public Produit search(Integer id) {
		return produitDao.getOne(id);
	}

	@Override
	public void create(Produit produit) {
		produitDao.create(produit);
		
	}

	@Override
	public void modify(Produit produit) {
		produitDao.modify(produit);
		
	}

	@Override
	public void delete(Integer id) {
		produitDao.delete(id);
	}

	public IProduitDao getProduitDao() {
		return produitDao;
	}

	public void setProduitDao(IProduitDao produitDao) {
		this.produitDao = produitDao;
	}

	
	
}
