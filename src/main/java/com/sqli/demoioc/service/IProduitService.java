package com.sqli.demoioc.service;

import java.util.List;

import com.sqli.demoioc.model.Produit;

public interface IProduitService {
	
	List<Produit> getAll();
	void create(Produit produit);
	void modify(Produit produit);
	void delete(Integer id);
	Produit search(Integer id);
}
