package com.sqli.demoioc;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sqli.demoioc.model.Produit;
import com.sqli.demoioc.service.IProduitService;

public class App {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext(
				"applicationContext.xml");

		IProduitService produitService = context.getBean(IProduitService.class);

		List<Produit> produits = produitService.getAll();

		for (Produit produit : produits) {
			System.out.println("id :" + produit.getId());
			System.out.println("libelle :" + produit.getLibelle());
			System.out.println("Prix :" + produit.getPrix());
			System.out.println(" ------------- ");
		}

	}
}
