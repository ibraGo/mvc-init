package com.sqli.demoioc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sqli.demoioc.model.Produit;
import com.sqli.demoioc.service.IProduitService;

@Controller
public class ProduitController {

	@Autowired
	private IProduitService produitService;

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(ModelAndView pModel) {
		pModel.setViewName("produitFormulaire");
		Produit produit = new Produit();
		pModel.addObject("produit", produit);
		return pModel;
	}

	@RequestMapping(value = "/saveEdit", method = RequestMethod.POST)
	public String saveEdit(@ModelAttribute Produit pProduit, BindingResult result) {
		produitService.modify(pProduit);
		return "redirect:/";
	}

	@RequestMapping(value = "/delete/{productId}", method = RequestMethod.GET)
	public String delete(@PathVariable("productId") int id, ModelAndView pModel) {
		produitService.delete(id);
		return "redirect:/";
	}

	@RequestMapping(value = "/edit/{productId}", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable("productId") int id, ModelAndView pModel) {
		pModel.setViewName("modifierProduit");
		pModel.addObject("produit", produitService.search(id));
		return pModel;
	}

	@RequestMapping(value = "/ajouterProduit", method = RequestMethod.POST)
	public String save(@ModelAttribute Produit pProduit, BindingResult result) {
		produitService.create(pProduit);
		return "redirect:/";
	}

	@RequestMapping(value = "/")
	public ModelAndView listerProduits(ModelAndView pModel) {
		pModel.setViewName("listproduit");
		pModel.addObject("produits", produitService.getAll());
		return pModel;
	}

}
