package com.sqli.demoioc.dao;

import java.util.List;

import com.sqli.demoioc.model.Produit;

public interface IProduitDao {

	void create(Produit produit);
	void modify(Produit produit);
	List<Produit> getAll();
	Produit getOne(Integer id);
	void delete(Integer id);
}
