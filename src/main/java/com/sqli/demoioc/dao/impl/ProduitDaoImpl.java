package com.sqli.demoioc.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.sqli.demoioc.constants.TypeDeProduit;
import com.sqli.demoioc.dao.IProduitDao;
import com.sqli.demoioc.model.Produit;

@Component
public class ProduitDaoImpl implements IProduitDao {

	private static Map<Integer, Produit> list;

	static {
		list = new HashMap<Integer, Produit>();

		list.put(1, new Produit(1, "fromage", 15.00, TypeDeProduit.LAITIER));
		list.put(2, new Produit(2, "abricot", 32.00, TypeDeProduit.LEGUMES_FRUITS));
		list.put(3, new Produit(3, "farine", 100.00, TypeDeProduit.ALIMENTATION));
		list.put(4, new Produit(4, "gel de cheveau", 20.00, TypeDeProduit.COSMETIQUE));
	}

	@Override
	public List<Produit> getAll() {

		return new ArrayList<Produit>(list.values());
	}

	@Override
	public Produit getOne(Integer id) {

		return list.get(id);
	}

	@Override
	public void create(Produit produit) {
		produit.setId(list.size() + 1);
		list.put(produit.getId(), produit);

	}

	@Override
	public void modify(Produit produit) {
		list.put(produit.getId(), produit);

	}

	@Override
	public void delete(Integer id) {
		list.remove(id);

	}

}
